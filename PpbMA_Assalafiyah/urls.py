from django.contrib import admin
from django.urls import path, include

from . import h1


urlpatterns = [
    path('admin/', admin.site.urls),
    path('Beranda/', h1.Beranda),
    path('ortu/', h1.ortu),
    path('asal/', h1.asal),
    path('', h1.index),
    path('datasiswi/', h1.datasiswi),
    path('Daftar/', h1.Daftar),
    path('PPB_Online/', include('PPB_Online.urls')),
]
