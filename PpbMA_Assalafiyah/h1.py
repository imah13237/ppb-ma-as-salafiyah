from django.http import HttpResponse
from django.shortcuts import render


def index(request):
    return render(request, 'index.html')

def datasiswi(request):
    return render(request, 'datasiswi.html')

def Beranda(request):
    return render(request, 'Beranda.html')

def ortu(request):
    return render(request, 'ortu.html')

def Daftar(request):
    return render(request, 'Daftar.html')

def asal(request):
    return render(request, 'AsalSekolah.html')


