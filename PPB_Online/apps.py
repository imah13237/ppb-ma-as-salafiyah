from django.apps import AppConfig


class PpbOnlineConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PPB_Online'
